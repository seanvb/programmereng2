﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMI_Berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoeveel weeg je in kg?");
            double mass = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double height = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"Je BMI bedraagt {mass / (height * height):F2}.");

        }
    }
}
