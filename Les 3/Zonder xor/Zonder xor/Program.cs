﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zonder_xor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een eerste getal?");
            int number1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef een tweede getal?");
            int number2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef nummer van de bit (te tellen vanaf de kleinste) ? ");
           
            int shift = Convert.ToInt32(Console.ReadLine()) - 1;
            int shifted1 = (number1 >> shift);
            int shifted2 = (number2 >> shift);
            // ~ heeft voorrang op & heeft voorrang op |
            Console.WriteLine($"Precies één bit met waarde 1:{ Convert.ToBoolean((shifted1 & ~shifted2 | ~shifted1 & shifted2) & 1)}");
 }
    }
}
