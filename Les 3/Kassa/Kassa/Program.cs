﻿using System;

namespace Kassa
{
    class Program
    {
        static void Main(string[] args)
        {
            const float Frieten = 20f;
            const float Hapjes = 10f;
            const float Ijs = 3f; // pascal notatie
            const float Drank = 2f; // constanten variable, waarde verandert niet.
            float totaal = 0f;

            Console.WriteLine("mosselen met frietjes?");
            totaal = Frieten * float.Parse(Console.ReadLine());
            Console.WriteLine("konigingenhapjes?");
            totaal += Hapjes * float.Parse(Console.ReadLine());
            Console.WriteLine("ijsjes?");
            totaal += Ijs * float.Parse(Console.ReadLine());
            Console.WriteLine("dranken?");
            totaal += Drank * float.Parse(Console.ReadLine());

            Console.WriteLine($"Het totaal te betalen bedrag in {totaal} EURO");
            Console.ReadLine();
        }
    }
}
