﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orakeltje
{
    class Program
    {
        static void Main(string[] args)
        {
            Random randomGen = new Random();
            int number = randomGen.Next(5, 126);
            Console.WriteLine($"Je zal nog {number} jaar leven.");
        }

    }
}
