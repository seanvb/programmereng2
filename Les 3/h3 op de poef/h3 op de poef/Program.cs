﻿using System;

namespace h3_op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voer een bedrag in");
            float bedrag = Int32.Parse(Console.ReadLine());
            Console.WriteLine($"De poef staat op {bedrag} euro. \n Voer een bedrag in.");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {bedrag} euro. \n Voer een bedrag in.");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {bedrag} euro. \n Voer een bedrag in.");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De poef staat op {bedrag} euro. \n Voer een bedrag in.");
            bedrag += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("*************************");
            Console.WriteLine($"De poef staat op {bedrag} euro.");
            float afbetaling = bedrag / 10f;
            float rest = bedrag % 10;
            Console.WriteLine(@$"Het totaal van de poef is {bedrag} euro
            Dit zal {Math.Round(afbetaling, 2)} afbetalingen vragen.
            De laatste keer betaal je {rest}");
            Console.ReadLine();


        }
    }
}
