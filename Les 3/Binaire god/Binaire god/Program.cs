﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Binaire_god
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een getal.");
            int number = Convert.ToInt32(Console.ReadLine());
            number = number & ~1;
            Console.WriteLine($"Het getal is nu {number}.");
            number = number << 3;
            Console.WriteLine($"Het getal is nu {number}.");
            Console.WriteLine("Geef een binaire string.");
            number = Convert.ToInt32(Console.ReadLine(), 2);
            number = number & ~1;
            Console.WriteLine($"Het getal is nu {Convert.ToString(number, 2)}.");
            number = number << 3;
            Console.WriteLine($"Het getal is nu {Convert.ToString(number, 2)}.");
        }

    }
}
