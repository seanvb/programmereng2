﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feestkassa
{
    class Program
    {
        static void Main(string[] args)
        {
            int count, sum;
            sum = 0;
            Console.WriteLine("Frietjes?");
            count = Convert.ToInt32(Console.ReadLine());
            sum += count * 20;
            Console.WriteLine("Koninginnenhapjes?");
            count = Convert.ToInt32(Console.ReadLine());
            sum += count * 10;
            Console.WriteLine("Ijsjes?");
            count = Convert.ToInt32(Console.ReadLine());
            sum += count * 3;
            Console.WriteLine("Dranken?");
            count = Convert.ToInt32(Console.ReadLine());
            sum += count * 2;
            Console.WriteLine($"Het totaal bedrag is {sum} euro.");
        }
    }
}
