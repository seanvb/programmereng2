﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maaltafels_binair
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ranGen = new Random();
            int multiplier;
            int randomNumber;
            multiplier = 1;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear();
            Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is { Convert.ToString(multiplier * randomNumber, 2)}");
            
            Console.ReadLine();
            multiplier = 2;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear();
            Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2)}");
            
            Console.ReadLine();
            multiplier = 3;
            randomNumber = ranGen.Next(1, 11);
            Console.Clear();
            Console.WriteLine($"{Convert.ToString(multiplier, 2)} * {Convert.ToString(randomNumber, 2)} is {Convert.ToString(multiplier * randomNumber, 2)}");
            
            Console.ReadLine();
            // en zo verder tot 10
        }
    }
}
