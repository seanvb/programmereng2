﻿using System;

namespace Tussentijdse_Evaluatie_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // GEHEUGEN

            double snickers = 1.50;
            double mars = 1.20;
            double magnum = 3.00;

            double tweeEuro = 2.00;
            double vijfEuro = 5.00;
            var x = 0.00;

            // INTRO

            Console.WriteLine("Welkom bij de snoepjesautomaat!");
            Console.WriteLine("Hieronder vind u de keuze uit de producten.");
            Console.WriteLine($"- Snickers: {snickers} euro");
            Console.WriteLine($"- Mars: {mars} euro");
            Console.WriteLine($"- Magnum: {magnum} euro");

            // EERSTE INGAVE

            Console.ReadLine();
            Console.WriteLine("Wat zal je ingeven?");
            Console.ReadLine();
            Console.WriteLine("Er wordt 5 euro ingegeven");
            Console.ReadLine();
            Console.WriteLine("Je hebt gekozen voor een Magnum.");

            // IF ELSE STATEMENT 1

            if (magnum <= vijfEuro)
            {
                x = vijfEuro - magnum;
                Console.WriteLine($"Je krijgt {x} euro terug.");
            }

            else {
            
                Console.WriteLine("Je hebt niet genoeg gegeven.");

            }

            // TWEEDE INGAVE

            Console.ReadLine();
            Console.WriteLine("Wat zal je ingeven?");
            Console.ReadLine();
            Console.WriteLine("Er wordt 2 euro ingegeven");
            Console.ReadLine();
            Console.WriteLine("Je hebt gekozen voor een Magnum.");

            // IF ELSE STATEMENT 2

            if (magnum < tweeEuro)
            {
                x = tweeEuro - magnum;
                Console.WriteLine($"Je krijgt {x} terug.");
            }

            else
            {
                x = tweeEuro - magnum;
                Console.WriteLine($"Je hebt niet genoeg gegeven en komt {x} euro te kort.");
            }

            // EINDE

            Console.ReadLine();


        }
    }
}
