﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            const int cost = 20;
            const int reducedCost = 10;
            int discountFrom;
            Console.WriteLine("Vanaf welk aantal geldt de korting?");
            discountFrom = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Hoeveel paar schoenen wil je kopen?");
            int pairs = Convert.ToInt32(Console.ReadLine());
            if (pairs < discountFrom)
            {
                Console.WriteLine($"Je moet {pairs * cost} euro betalen.");
            }
            else
            {
                Console.WriteLine($"Je moet {pairs * reducedCost} euro betalen.");
            }

        }
    }
