﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ohm_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat wil je berekenen? spanning, weerstand of stroomsterkte ? ");

            string computedUnit = Console.ReadLine();
            if (computedUnit == "spanning")
            {
                Console.WriteLine("Wat is de weerstand?");
                double resistance = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Wat is de stroomsterkte?");
                double current = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine($"De spanning bedraagt {resistance * current}.");
            }
            else if (computedUnit == "weerstand")
            {
                Console.WriteLine("Wat is de spanning?");
                double voltage = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Wat is de stroomsterkte?");
                double current = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine($"De weerstand bedraagt {voltage / current}.");
            }
            else if (computedUnit == "stroomsterkte")
            {
                Console.WriteLine("Wat is de spanning?");
                double voltage = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Wat is de weerstand?");
                double resistance = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine($"De stroomsterkte bedraagt {voltage / resistance}.");
            }
            else
            {
                Console.WriteLine("Ongeldig antwoord!");
            }
        }           
    }
}
