﻿using System;

class Program
{
    static void Main(string[] args)
    {
        
        // INTRODUCTIE

        Console.WriteLine("Welkom op mijn eerste dobbelspel gemaakt met C#!");
        Console.WriteLine("Laten we al snel beginnen met het spel.");
        Console.WriteLine();
        Console.WriteLine("Het spel is simpel. Beide spelers dobbelen hun dobbelstenen en het hoogte getal wint. \nWe houden het spannend door 3 rondes toe te voegen.");
        Console.WriteLine("Aan de hand van het eindtotaal zullen we gezien wie er gewonnen heeft.");
        Console.WriteLine();
        Console.WriteLine("Druk op Enter om verder te gaan ...");
        Console.ReadLine();
        Console.Clear();

        // SPELERS

        Console.WriteLine("Wat is de naam van Speler 1?");

        string speler1 = Console.ReadLine();

        Console.WriteLine();
        Console.WriteLine("Wat is de naam van Speler 2?");

        string speler2 = Console.ReadLine();

        Console.WriteLine();
        Console.WriteLine("Druk op Enter om te rollen");
        Console.ReadLine();


        // RONDE 1

        Console.WriteLine("===== RONDE 1 =====");
        Console.WriteLine();

        Random rnd = new Random();

        int dice1 = rnd.Next(1, 7);
        int dice2 = rnd.Next(1, 7);

        Console.WriteLine(speler1 + " heeft " + dice1 + " gerold.");
        Console.WriteLine(speler2 + " heeft " + dice2+ " gerold.");

        if (dice1 > dice2)
        {
            Console.WriteLine(speler1 + " heeft gewonnen!");
        }
        else if (dice2 > dice1)
        {
            Console.WriteLine(speler2 + " heeft gewonnen!");
        }
        else
        {
            Console.WriteLine("Gelijkspel!");
        }

        Console.WriteLine();
        Console.WriteLine("Druk op Enter om te rollen");
        Console.ReadLine();

        // RONDE 2

        Console.WriteLine("===== RONDE 2 =====");
        Console.WriteLine();

        int dice3 = rnd.Next(1, 7);
        int dice4 = rnd.Next(1, 7);

        Console.WriteLine(speler1 + " heeft " + dice3 + " gerold.");
        Console.WriteLine(speler2 + " heeft " + dice4 + " gerold.");

        if (dice3 > dice4)
        {
            Console.WriteLine(speler1 + " heeft gewonnen!");
        }
        else if (dice3 > dice4)
        {
            Console.WriteLine(speler2 + " heeft gewonnen!");
        }
        else
        {
            Console.WriteLine("Gelijkspel!");
        }

        Console.WriteLine();
        Console.WriteLine("Druk op Enter om te rollen");
        Console.ReadLine();

        // RONDE 3

        Console.WriteLine("===== FINALE =====");
        Console.WriteLine();

        int dice5 = rnd.Next(1, 7);
        int dice6 = rnd.Next(1, 7);

        Console.WriteLine(speler1 + " heeft " + dice5 + " gerold.");
        Console.WriteLine(speler2 + " heeft " + dice6 + " gerold.");

        if (dice5 > dice6)
        {
            Console.WriteLine(speler1 + " heeft gewonnen!");
        }
        else if (dice5 > dice6)
        {
            Console.WriteLine(speler2 + " heeft gewonnen!");
        }
        else
        {
            Console.WriteLine("Gelijkspel!");
        }

        Console.ReadLine();
        Console.Clear();
        Console.WriteLine("Bedankt om te spelen!");
        Console.ReadLine();


    }
}
