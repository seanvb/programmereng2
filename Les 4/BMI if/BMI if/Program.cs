﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMI_if
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel weeg je in kg?");
            double mass = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double height = Convert.ToDouble(Console.ReadLine());
            double bmi = mass / (height * height);
            Console.WriteLine($"Je BMI bedraagt {bmi:F2}.");
            if (bmi < 18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ondergewicht");
            }
            else if (bmi < 25)
            {
                Console.WriteLine("normaal gewicht");
            }
            else if (bmi < 30)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("overgewicht");
            }
            else if (bmi < 40)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("zwaarlijvig");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("ernstige obesitas");
            }
            Console.ResetColor();
        }
    }
}
