﻿using System;

namespace h3_op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {

            Random getal = new Random();
            float toevalsGetal = 0f;
            float totaal = 0f;


            toevalsGetal = getal.Next(1, 51); //Genereert een getal tussen 1 en 51 en slaagt die op als een variable.
            Console.WriteLine($"Voer een bedrag in: \n Automatisch gegenereerd: {toevalsGetal}"); //Het automatisch gegenereerd bedrag (toevalsGetal) wordt weergegeven (Console.WriteLine).
            totaal = toevalsGetal; //Variable (totaal) decladeert het eerste totaal.
            Console.WriteLine($"De poef staat op {totaal}");

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"Voer een bedrag in: \n Automatisch gegenereerd: {toevalsGetal}");
            totaal += toevalsGetal; //Variable (totaal) decladeert het volgende totaal.
            Console.WriteLine($"De poef staat op {totaal}");

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"Voer een bedrag in: \n Automatisch gegenereerd: {toevalsGetal}");
            totaal += toevalsGetal;
            Console.WriteLine($"De poef staat op {totaal}");

            toevalsGetal = getal.Next(1, 51);
            Console.WriteLine($"Voer een bedrag in: \n Automatisch gegenereerd: {toevalsGetal}");
            totaal += toevalsGetal;
            Console.WriteLine($"De poef staat op {totaal}");

            Console.WriteLine("********************************");

            Console.WriteLine($"Het totaal is {totaal}");

            Console.ReadLine();


        }
    }
}
