﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Learning
{
    class User
    {
        public string Name;
        public void GreetingAsk()
        {
            Console.WriteLine($"Enter your name and press Enter to start.");
            Name = Console.ReadLine();
            Console.Clear();
        }
        public void GreetingAnswer()
        {
            Console.WriteLine($"Hello ...");
            Console.WriteLine();
            Thread.Sleep(2000);
            Console.WriteLine($"My name is Delta.");
            Thread.Sleep(2000);
            Console.Write($"I am a Program which will guide you through this ...");
            Thread.Sleep(3000);
            Console.Write($"Game.");
            Console.WriteLine();
            Thread.Sleep(1000);
            Console.WriteLine($"So, you are called {Name} ...");
            Thread.Sleep(3000);
            Console.WriteLine($"That's a beautiful name.");
            Thread.Sleep(2000);
            Console.WriteLine();
            Console.WriteLine($"I hope you also have beautiful brains because you'll need them to finish this ... game.");
            Thread.Sleep(5000);
            Console.Clear();


        }

    }
    class Questions
    {
        public string Q1 = "Who was the legendary Benedictine monk who invented champagne?";
        public string Q1A = "A. Dom Perignon";
        public string Q1B = "B. Pièrre Darmont";
        public string Q1C = "C. Saby Sauvignon";

        public string Q2 = "Name the world's biggest island.";
        public string Q2A = "A. Australia";
        public string Q2B = "B. Greenland";
        public string Q2C = "C. Madagascar";

        public string Q3 = "What kind of weapon is a falchion?";
        public string Q3A = "A. Sword";
        public string Q3B = "B. Dagger";
        public string Q3C = "C. Spear";

        public string Q4 = "Name the seventh planet from the sun.";
        public string Q4A = "A. Neptune";
        public string Q4B = "B. Uranus";
        public string Q4C = "C. Jupiter";

        public string Q5 = "What is the capital city of Spain?";
        public string Q5A = "A. Barcelona";
        public string Q5B = "B. Toledo";
        public string Q5C = "C. Madrid";

        public string Q6 = "When did World War II end?";
        public string Q6A = "A. 1944";
        public string Q6B = "B. 1945";
        public string Q6C = "C. 1946";

        public string Q7 = "Name the director of the Lord of the Rings trilogy.";
        public string Q7A = "A. Peter Jackson";
        public string Q7B = "B. Jack Peterson";
        public string Q7C = "C. Peter Blackson";

        public string Q8 = "Which nutrient has the most calories per gram?";
        public string Q8A = "A. Carbs";
        public string Q8B = "B. Proteins";
        public string Q8C = "C. Fats";

        public string Q9 = "When was the euro introduced as legal currency on the world market?";
        public string Q9A = "A. 1999";
        public string Q9B = "B. 2000";
        public string Q9C = "C. 2001";

        public string Q10 = "What is the capital city of Europe?";
        public string Q10A = "A. Berlin";
        public string Q10B = "B. Paris";
        public string Q10C = "C. Brussels";

        // PROBLEM STILL NOT FIXED!
        // totalScore won't add correctly

        public string input;

        public int maxScore = 10;

        public void Quiz()
        {
            // QUESTION 1
            Console.WriteLine("Question #1");
            Console.WriteLine();

            Console.WriteLine(Q1);
            Console.WriteLine(Q1A);
            Console.WriteLine(Q1B);
            Console.WriteLine(Q1C);

            input = Console.ReadLine();

            if (input == "a")
            {
                Console.WriteLine("Correct!");
            } else {
                Console.WriteLine("WRONG!");
                maxScore--;

            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 2
            Console.WriteLine("Question #2");
            Console.WriteLine();

            Console.WriteLine(Q2);
            Console.WriteLine(Q2A);
            Console.WriteLine(Q2B);
            Console.WriteLine(Q2C);

            input = Console.ReadLine();

            if (input == "b")
            {
                Console.WriteLine("Correct!");
            } else {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 3
            Console.WriteLine("Question #3");
            Console.WriteLine();

            Console.WriteLine(Q3);
            Console.WriteLine(Q3A);
            Console.WriteLine(Q3B);
            Console.WriteLine(Q3C);

            input = Console.ReadLine();

            if (input == "a")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 4
            Console.WriteLine("Question #4");
            Console.WriteLine();

            Console.WriteLine(Q4);
            Console.WriteLine(Q4A);
            Console.WriteLine(Q4B);
            Console.WriteLine(Q4C);

            input = Console.ReadLine();

            if (input == "b")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 5
            Console.WriteLine("Question #5");
            Console.WriteLine();

            Console.WriteLine(Q5);
            Console.WriteLine(Q5A);
            Console.WriteLine(Q5B);
            Console.WriteLine(Q5C);

            input = Console.ReadLine();

            if (input == "c")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 6
            Console.WriteLine("Question #6");
            Console.WriteLine();

            Console.WriteLine(Q6);
            Console.WriteLine(Q6A);
            Console.WriteLine(Q6B);
            Console.WriteLine(Q6C);

            input = Console.ReadLine();

            if (input == "b")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 7
            Console.WriteLine("Question #7");
            Console.WriteLine();

            Console.WriteLine(Q7);
            Console.WriteLine(Q7A);
            Console.WriteLine(Q7B);
            Console.WriteLine(Q7C);

            input = Console.ReadLine();

            if (input == "a")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 8
            Console.WriteLine("Question #8");
            Console.WriteLine();

            Console.WriteLine(Q8);
            Console.WriteLine(Q8A);
            Console.WriteLine(Q8B);
            Console.WriteLine(Q8C);

            input = Console.ReadLine();

            if (input == "c")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 9
            Console.WriteLine("Question #9");
            Console.WriteLine();

            Console.WriteLine(Q9);
            Console.WriteLine(Q9A);
            Console.WriteLine(Q9B);
            Console.WriteLine(Q9C);

            input = Console.ReadLine();

            if (input == "a")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();

            // QUESTION 10
            Console.WriteLine("Question #10");
            Console.WriteLine();

            Console.WriteLine(Q10);
            Console.WriteLine(Q10A);
            Console.WriteLine(Q10B);
            Console.WriteLine(Q10C);

            input = Console.ReadLine();

            if (input == "c")
            {
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine("WRONG!");
                maxScore--;
            }

            Thread.Sleep(1000);
            Console.Clear();
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            User user = new User();
            user.GreetingAsk();
            user.GreetingAnswer();

            Questions questions = new Questions();
            questions.Quiz();

            Console.WriteLine("Game has finished!");
            Thread.Sleep(1000);
            Console.WriteLine($"Your score is {questions.maxScore}/10");
            Thread.Sleep(2000);
            Console.WriteLine($"Thanks for playing!");
            Console.WriteLine();




        }

    }
}
