﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberGuesser
{
    class Program
    {
        static void Main(string[] args)
        {
            GetAppInfo(); // Run GetAppInfo to get info

            GreetUser(); // Ask for users name and greet
            
            while (true)
            {


                // Set correct number
                // int correctNumber = 7;

                // Create a new Random object
                Random random = new Random();

                //Init guess var
                int correctNumber = random.Next(1, 10);

                int guess = 0;

                // Ask user for number
                Console.WriteLine("Guess a number between 1 and 10.");

                while(guess != correctNumber)
                {
                    // Get users input
                    string input = Console.ReadLine();

                    // Make sure it's a number
                    if(!int.TryParse(input, out guess))
                    {
                        // Print error message
                        PrintColorMessage(ConsoleColor.Red, "Please use an actual number");

                        // Keep going
                        continue;

                    }

                    // Cast to int and put in guess
                    guess = Int32.Parse(input);

                    // Match guess to correct number
                    if(guess != correctNumber)
                    {
                        // Print error message
                        PrintColorMessage(ConsoleColor.Red, "Wrong number, please try again.");


                    }
                }

                // Change text color
                Console.ForegroundColor = ConsoleColor.Yellow;

                // Tell users it's the right number
                Console.WriteLine("You are CORRECT!!!");

                // Reset text color
                Console.ResetColor();

                // Print error message
                PrintColorMessage(ConsoleColor.Yellow, "CORRECT!");


                // Ask to play again

                Console.WriteLine("Play again? [Y or N]");

                // Get answer
                string answer = Console.ReadLine().ToUpper();

                if (answer == "Y")
                {

                }
                else if(answer == "N")
                {
                    return;
                }
                else
                {
                    return;
                }

            }
        }

        // Get and display app info
        static void GetAppInfo()
        {
            string appName = "Number Guesser";
            string appVersion = "1.0.0";
            string appAuthor = "Sean Van Bortel";

            // Change text color
            Console.ForegroundColor = ConsoleColor.Green;

            // Write out app info
            Console.WriteLine("{0}: Version {1} by {2}", appName, appVersion, appAuthor);

            // Reset text color
            Console.ResetColor();

        }

        // Ask users name and greet
        static void GreetUser()
        {
            // Ask users name

            Console.WriteLine("What is your name?");

            // Get user input
            string inputName = Console.ReadLine();

            Console.WriteLine("Hello {0}, let's play a game...", inputName);

        }

        // Print color message
        static void PrintColorMessage(ConsoleColor color, string message)
        {
            // Change text color
            Console.ForegroundColor = color;

            // Tell users it's not a number
            Console.WriteLine(message);

            // Reset text color
            Console.ResetColor();

        }
    }
}
