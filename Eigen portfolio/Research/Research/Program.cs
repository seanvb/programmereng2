﻿using System;
using System.Threading;

namespace Research
{

    class Program
    {
        static void Main(string[] args)
        {

            int num = 60;

            while(num > 0)
            {


                if (num != 1)
            {
                --num;
                    Console.WriteLine($"{num} sec remaining ...");
                    Thread.Sleep(1000);
                    Console.Clear();
            }
            else if (num == 0)
            {
                Console.WriteLine("Time is out!");
            }
            else
                {
                    break;
                }
            }

        }
    }
}
