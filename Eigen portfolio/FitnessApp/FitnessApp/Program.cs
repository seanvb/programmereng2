﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Here you can log your food information during a weight gain or loss diet. \nIt's quiet simple, the program will ask you how many carbs, proteins and fat the food has \nand it will calculate your total result.");
            Console.WriteLine();
            Console.WriteLine("Press enter to continue ...");
            Console.ReadLine();
            Console.WriteLine("Enter your macros per 100gr ...");
            Console.WriteLine("How many kcal?");
            double Kcal = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("How many carbs");
            double Carbs = Convert.ToDouble(Console.ReadLine());


            Console.WriteLine("How many proteins");
            double Proteins = Convert.ToDouble(Console.ReadLine());


            Console.WriteLine("How much fat?");
            double Fat = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine($"Your total is {Kcal} kcal (per 100gr):");
            Console.WriteLine($"- {Carbs}gr carbs.");
            Console.WriteLine($"- {Proteins}gr proteins.");
            Console.WriteLine($"- {Fat}gr fat.");

            Console.WriteLine();
            Console.WriteLine("How much did you eat?");
            Console.WriteLine("Enter in grams ...");
            double Consumed = Convert.ToDouble(Console.ReadLine());
            double totalConsumedKcal = (Kcal / 100) * Consumed;
            Math.Round(totalConsumedKcal, 0);

            double totalConsumedCarbs = (Carbs / 100) * Consumed;
            Math.Round(totalConsumedCarbs, 0);

            double totalConsumedProteins = (Proteins / 100) * Consumed;
            Math.Round(totalConsumedProteins, 0);

            double totalConsumedFat = (Fat / 100) * Consumed;
            Math.Round(totalConsumedFat, 0);


            Console.WriteLine();
            Console.WriteLine("================================================================");
            Console.WriteLine($"This is your total for {Consumed}gr:");
            Console.WriteLine();
            Console.WriteLine($"- {totalConsumedKcal} kcal.");
            Console.WriteLine($"- {totalConsumedCarbs} carbs.");
            Console.WriteLine($"- {totalConsumedProteins} proteins.");
            Console.WriteLine($"- {totalConsumedFat} fat.");
            Console.WriteLine("================================================================");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Thanks for testing this app.");
            Console.WriteLine("Version 0.1");

            Console.WriteLine("TEST END");



        }
    }
}
