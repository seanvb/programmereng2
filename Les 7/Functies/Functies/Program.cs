﻿using System;

namespace Functies
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met functies");
            Console.WriteLine("Keuze 1");
            Console.WriteLine("Keuze 2");
            Console.WriteLine("Keuze 3");

            Console.WriteLine("Maak uw keuze: ");

            string keuze = Console.ReadLine();
            switch (keuze)
            {
                case "1":
                    Console.WriteLine("Je hebt voor 1 gekozen.");
                    break;
                case "2":
                    Console.WriteLine("Je hebt voor 2 gekozen.");
                    break;
                case "3":
                    Console.WriteLine("Je hebt voor 3 gekozen.");
                    break;
            }

            Zeg(keuze);

            //programma die het grootste getal van twee ingegeven getallen berekent
            double getal = 1;
            while (getal != 0)
            {
                Console.WriteLine("Geef een getal in: ");
                string input = Console.ReadLine();
                getal = -1;
                if (input.Length != 0)
                {
                    Convert.ToDouble(Console.ReadLine());
                }
                else
                {
                    Console.WriteLine($"Het eerste getal {getal} is groter dan het tweede getal {getal2}.");
                }
            }

            double getal1 = InputGetal();
            double getal2 = InputGetal();
        }

        static double InputGetal()
        {
            Console.WriteLine("Geef een getal in: ");
            double getal = Convert.ToDouble(Console.ReadLine());
            return getal;

        }
        static void Zeg(string keuze)
        {
            Console.WriteLine($"Je hebt voor {keuze} gekozen.");
        }

    }
}
