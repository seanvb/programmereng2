﻿using System;

namespace Functies2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Functies");
            double eersteGetal = inputGetal();
            double tweedeGetal = inputGetal();
            Console.WriteLine($"Het grootste getal is {max(eersteGetal, tweedeGetal)}");
        }

        //methoden (functies)

        static double inputGetal()
            {
            Console.WriteLine("Typ een getal in: ");
            double getal = Convert.ToDouble(Console.ReadLine());
            return getal;
        }

        static double max(double getal1, double getal2)
        {
            if (getal1 < getal2)
            {
                return getal2;
            }
            else
            {
                return getal1;
            }
        }
    }
}
