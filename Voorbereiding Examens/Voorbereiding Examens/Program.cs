﻿using System;

namespace VoorbereidingExamen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Voorbereiding examen");
            //string[] voornamen = VulEenArrayMetVoornamen();
            //string alleVoornamen = PrintStringArray(voornamen);
            //Console.WriteLine(alleVoornamen);

            Console.WriteLine(PrintStringArray(VulEenArrayMetVoornamenEnTest()));
            Console.WriteLine("Druk op een toets om de console te sluiten...");
            Console.ReadKey();

        }

        static string[] VulEenArrayMetVoornamen()
        {
            const int MaxAantal = 2;
            string[] arrayMetVoornamen = new string[MaxAantal];

            for (int i = 0; i < MaxAantal; i++)
            {
                Console.WriteLine("Typ een voornaam in: ");
                string voornaam = Console.ReadLine();
                arrayMetVoornamen[i] = voornaam;
            }

            return arrayMetVoornamen;
        }

        static string PrintStringArray(string[] array)
        {
            string tekst = string.Empty;
            // een for als je van te voren precies weet
            // hoeveel keren
            for (int i = 0; i <= array.Length - 1; i++)
            {
                tekst += $"{array[i]}, ";
            }
            return tekst.Substring(0, tekst.Length - 2);
        }

        static string[] VulEenArrayMetVoornamenEnTest()
        {
            const int MaxAantal = 2;
            string[] arrayMetVoornamen = new string[MaxAantal];
            // sentinel /wachter
            int juisteVoornamen = 0;
            // while als je niet van te voren weet hoeveel keren
            while (juisteVoornamen < MaxAantal)
            {
                Console.WriteLine("Typ een voornaam in: ");
                string voornaam = Console.ReadLine();
                if (voornaam.Length > 0)
                {
                    arrayMetVoornamen[juisteVoornamen] = voornaam;
                    juisteVoornamen++;
                }
            }

            return arrayMetVoornamen;
        }
    }
}
