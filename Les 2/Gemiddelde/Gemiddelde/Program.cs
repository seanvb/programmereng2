﻿using System;
namespace _05_H1_gemiddelde
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Een beetje wiskunde: gemiddelde");
            Console.WriteLine();
            Console.WriteLine("Berekening gemiddelde met gebruik van integers");
            Console.WriteLine((18 + 11 + 8) / 3);
            Console.WriteLine();
            Console.WriteLine("Berekening gemiddelde met gebruik van floats");
            Console.WriteLine((18.0f + 11 + 8) / 3);
            // Wacht op een toetsaanslag vooraleer het programma af te sluiten.
            Console.ReadKey();
        }
    }
}