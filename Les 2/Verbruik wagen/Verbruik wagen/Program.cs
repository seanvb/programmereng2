﻿using System;
namespace _03_H1_verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            // declareer de variabelen met initialisatie
            double kilometerstandBijAanvang = 0d;
            double kilometerstandBijAankomst = 0d;
            double aantalLiterinTankBijVertrek = 0d;
            double aantalLiterinTankBijAankomst = 0d;
            double verbruik = 0d;
            // Vraag alle info op aan gebruiker en bewaar
            Console.Write("Geef het aantal liter in tank voor de rit: ");
            aantalLiterinTankBijVertrek = double.Parse(Console.ReadLine());
            Console.Write("Geef het aantal liter in tank na de rit: ");
            aantalLiterinTankBijAankomst = double.Parse(Console.ReadLine());
            Console.Write("Geef kilometerstand van je auto voor de rit: ");
            kilometerstandBijAanvang = double.Parse(Console.ReadLine());
            Console.Write("Geef kilometerstand van je auto na de rit: ");
            kilometerstandBijAankomst = double.Parse(Console.ReadLine());
            // bereken verbruik
            verbruik = 100 * (aantalLiterinTankBijVertrek -
            aantalLiterinTankBijAankomst) /
            (kilometerstandBijAankomst - kilometerstandBijAanvang);
            // verbruik weergeven
            Console.WriteLine($"Het verbruik van de auto is: {verbruik}");
            // afgerond:
            Console.WriteLine($"Het afgeronde verbruik van de auto is:");
        // Wacht op een toetsaanslag vooraleer het programma af te sluiten.
Console.ReadKey();
        }
    }
}