﻿using System;
namespace _06_H1_maaltafels
{
    class Program
    {
        static void Main(string[] args)
        {
            int baseNumber = 5;
            Console.Clear();
            Console.WriteLine("1 * " + baseNumber + " is " + (1 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("2 * " + baseNumber + " is " + (2 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("3 * " + baseNumber + " is " + (3 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("4 * " + baseNumber + " is " + (4 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("5 * " + baseNumber + " is " + (5 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("6 * " + baseNumber + " is " + (6 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("7 * " + baseNumber + " is " + (7 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("8 * " + baseNumber + " is " + (8 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("9 * " + baseNumber + " is " + (9 * baseNumber) +
            ".");
            Console.ReadLine();
            Console.Clear();
            Console.WriteLine("10 * " + baseNumber + " is " + (10 * baseNumber) +
            ".");
            // Wacht op een toetsaanslag vooraleer het programma af te sluiten.
            Console.ReadKey();
        }
    }
}