﻿using System;
namespace _07_H1_ruimte
{
    class Program
    {
        static void Main(string[] args)
        {
            // definieren variabelen
            float myWeight = 69.0f;
            Console.WriteLine($"Op Mercurius heb je een gewicht van {myWeight *0.38f}N.");

            Console.WriteLine($"Op Venus heb je een gewicht van {myWeight * 0.91f}N.");

            Console.WriteLine($"Op Aarde heb je een gewicht van {myWeight * 1.0f}N.");

            Console.WriteLine($"Op Mars heb je een gewicht van {myWeight * 0.38f}N.");

            Console.WriteLine($"Op Jupiter heb je een gewicht van {myWeight * 2.34f}N.");

            Console.WriteLine($"Op Saturnus heb je een gewicht van {myWeight *1.06f}N.");

            Console.WriteLine($"Op Uranus heb je een gewicht van {myWeight * 0.92f}N.");

            Console.WriteLine($"Op Neptunus heb je een gewicht van {myWeight *1.19f}N.");

            Console.WriteLine($"Op Pluto heb je een gewicht van {myWeight * 0.06f}N.");

            // Wacht op een toetsaanslag vooraleer het programma af te sluiten.

            Console.ReadKey();
        }
    }
}