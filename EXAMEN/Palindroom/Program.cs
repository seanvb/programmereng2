﻿using System;
using System.Threading;

namespace Palindroom
{
    class Program
    {
        static void Main(string[] args)
        {
            // TITEL
            Console.WriteLine("De applicatie controleert of een ingegeven woord een palindroom is of niet...");
            Thread.Sleep(3000);

            // UITLEG
            Console.WriteLine("Als je vijf palindromen hebt ingegeven, dan eindigt de applicatie.");
            Thread.Sleep(3000); // BEETJE ANIMATIE

            TekstOmgekeerd();
            TekstOmgekeerd();
            TekstOmgekeerd();
            TekstOmgekeerd();
            TekstOmgekeerd();

            PrintSystemInfo();

            Console.WriteLine("Applicatie werd beëindigd.");
            Thread.Sleep(3000);
            System.Environment.Exit(1);


            static void TekstOmgekeerd()
            {
                string input, omgekeerd = "";
                Console.WriteLine(); // PUUR ESTHETISCH
                Console.WriteLine("Geef een woord op om te controleren of het een palindroom is of niet: ");
                input = Console.ReadLine();

                string[] woordenLijst = new string[5];


                for (int i = input.Length - 1; i >= 0; i--) 
                {
                    omgekeerd += input[i].ToString();
                    // woordenLijst[i] = Console.ReadLine(); BESTE WAT IK ERVAN KON MAKEN :(
                }

                if (omgekeerd == input)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Het ingegeven woord is een palindroom");
                    Console.ResetColor();

                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Het ingegeven woord is GEEN palindroom");
                    Console.ResetColor();
                }


            }

            static void PrintSystemInfo()
            {
                Console.WriteLine();

                string systemName = System.Environment.MachineName;
                Console.WriteLine($"De naam van uw pc is {systemName}.");

                string userName = Environment.UserName;
                Console.WriteLine($"{userName} is uw gebruikersnaam.");

            }



        }
    }
}
