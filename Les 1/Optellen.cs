﻿using System;
namespace _02_H1_optellen
{
    class Program
    {
        static void Main(string[] args)
        {
            // Afspraak: denk aan de 'naming conventions'
            // variabelen beginnen met kleine letters & kies betekeningsvolle namen!!
            // Let op: denk goed na over het gebruikte datatype (niet te groot, maar ook niet te klein)
            // hou altijd controle en geef de variabelen een beginwaarde mee (wees controle - freak)
            int getal1 = 0;
            int getal2 = 0;
            int som = 0;
            //string invoer1 = string.Empty;
            //string invoer2 = string.Empty;
            string invoer = string.Empty;
            // Wijzig de kleur
            Console.ForegroundColor = ConsoleColor.Green;
            // invoer (inlezen getallen):
            Console.WriteLine("Geef het eerste getal: ");
            //invoer1 = Console.ReadLine();
            invoer = Console.ReadLine();
            getal1 = int.Parse(invoer);
            Console.WriteLine("Geef het tweede getal: ");
            //invoer2 = Console.ReadLine();
            invoer = Console.ReadLine();
            getal2 = int.Parse(invoer);
            // zet de tekst om naar een numerische waarde
            // 1e manier (oude schrijfwijze)
            //getal1 = Convert.ToInt32(invoer1);
            //getal2 = Convert.ToInt32(invoer2);
            // 2e manier (object oriented)
            //getal1 = int.Parse(invoer1);
            //getal2 = int.Parse(invoer2);
            // voer de berekening uit
            som = getal1 + getal2;
            // geef resultaat weer
            // 1e manier
            //Console.WriteLine("De som is: " + som);
            // 2e manier
            Console.WriteLine($"De som is: {som}");
            // Wacht op een toetsaanslag vooraleer het programma af te sluiten.
            Console.ReadKey();
        }
    }
}