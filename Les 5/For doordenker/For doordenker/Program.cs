﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace For_doordenker
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows = 4;
            for (int i = 1; i <= rows; ++i)
            {
                for (int j = 1; j <= i; ++j)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            for (int i = 1; i <= rows - 1; ++i)
            {
                for (int j = 1; j <= 4 - i % 4; ++j)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

    }
}
