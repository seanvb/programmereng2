﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rna_transscriptie
{
    class Program
    {
        static void Main(string[] args)
        {
            string inp = "", DNA = "", RNA = "";
            do
            {
                Console.WriteLine("Voer G,C,T of A in");
                inp = Console.ReadLine();
                switch (inp)
                {
                    case "G":
                        DNA += "G";
                        RNA += "C";
                        break;
                    case "C":
                        DNA += "C";
                        RNA += "G";
                        break;
                    case "T":
                        DNA += "T";
                        RNA += "A";
                        break;
                    case "A":
                        DNA += "A";
                        RNA += "U";
                        break;
                    default:
                        inp = "stop";
                        break;
                }
            } while (inp != "stop");
            Console.WriteLine($"\nResultaat:\n==========\n -DNA: {DNA}\n -RNA:{RNA}");
            Console.ReadKey();
        }

    }
}
