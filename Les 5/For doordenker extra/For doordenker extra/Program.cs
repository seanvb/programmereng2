﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace For_doordenker_extra
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows = 6;
            int k = 0;
            int space = 0;
            for (int i = 1; i <= rows; ++i, k = 0)
            {
                for (space = 1; space <= rows - i; ++space)
                {
                    Console.Write(" ");
                }
                while (k != 2 * i - 1)
                {
                    Console.Write("* ");
                    ++k;
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

    }
}
