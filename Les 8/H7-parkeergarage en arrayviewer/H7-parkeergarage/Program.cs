﻿using System;

namespace H7_parkeergarage
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Geef aantal auto's in:");
            int aantal = Convert.ToInt32(Console.ReadLine());

            double[] duur = new double[aantal];

            for (int i = 0; i < duur.Length; i++)
            {
                Console.WriteLine($"Geef parkeertijd auto {i + 1} in (uren)");
                duur[i] = Convert.ToDouble(Console.ReadLine());
            }

            ToonResultaat(duur);
        }

        static void ToonResultaat(double[] duur)
        {
            double somDuur = 0;
            double somKost = 0;
            Console.WriteLine("Auto\tDuur\tKost");
            for (int i = 0; i < duur.Length; i++)
            {
                double kost = berekenKosten(duur[i]);
                somKost += kost;
                somDuur += duur[i];

                Console.WriteLine($"{i + 1}\t{duur[i]}\t{berekenKosten(duur[i])}");
            }
            Console.WriteLine($"Totaal\t{somDuur}\t{somKost}");
        }

        static double berekenKosten(double duur)
        {
            double kost = 2;
            if (duur > 3)
            {
                double extra = Math.Ceiling(duur - 3);
                kost += (extra * 0.5);
            }
            if (duur >= 24)
            {
                kost = 10;
            }
            return kost;
        }
    }
}
