﻿using System;

namespace H7_arrayviewer
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayofints = { 1, 2, 3, 4, 5, 6, 7 };
            VisualiseerArray(arrayofints);
        }

        static void VisualiseerArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i]}\t");
            }
            Console.Write(Environment.NewLine);
        }
    }
}
