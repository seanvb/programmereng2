﻿using System;

namespace H7_opwarmers_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met de letters a tot z
            char[] letters = new char[26];
            int startascii = 97;

            for (int i = 0; i < letters.Length; i++)
            {
                letters[i] = (char)(startascii + i);
                Console.Write(letters[i] + ",");
            }

            Console.ReadKey();
        }
    }
}
