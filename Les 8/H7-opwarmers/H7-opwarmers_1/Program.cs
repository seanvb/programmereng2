﻿using System;
using System.Threading;

namespace H7_opwarmers_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met de getallen 0 tot 100
            int[] getallen = new int[10];

            for (int i = 0; i < 10; i++)
            {
                getallen[i] = i + 1;
                Console.Write(getallen[i] + ", ");
                Thread.Sleep(1000);

            }

            Console.ReadKey();
        }
    }
}
