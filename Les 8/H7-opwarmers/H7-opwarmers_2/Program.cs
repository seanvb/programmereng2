﻿using System;

namespace H7_opwarmers_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met de getallen van 100 tot 0
            int[] getallen = new int[100];
            int counter = 100;

            for (int i = 0; i < getallen.Length; i++)
            {
                getallen[i] = counter; //zonder counter: getallen[i]=100-i;
                counter--;
                Console.Write(getallen[i] + ",");
            }

            Console.ReadKey();
        }
    }
}
