﻿using System;

namespace H7_opwarmers_4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met willekeurige getallen tussen 1 en 100 (array is 20 lang)
            int[] getallen = new int[20];
            Random r = new Random();

            for (int i = 0; i < getallen.Length; i++)
            {
                getallen[i] = r.Next(1, 101);
                Console.Write(getallen[i] + ",");
            }

            Console.ReadKey();
        }
    }
}
