﻿using System;

namespace H7_opwarmers_5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Maak een array gevuld met afwisselen true en false(lengte is 30)
            bool[] binary = new bool[30];
            Random r = new Random();

            for (int i = 0; i < binary.Length; i++)
            {
                if (i % 2 == 0)
                    binary[i] = true;
                else
                    binary[i] = false;

                Console.Write(binary[i] + ",");
            }

            Console.ReadKey();
        }
    }
}
