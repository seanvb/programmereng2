﻿using System;

namespace H7_ceasar_encryptie
{
    class Program
    {
        static void Main(string[] args)
        {
            string words;
            char[] wordsarray;
            int i = 0, encryptkey;

            Console.Write("Geef de zin of woord(en) die moeten versleuteld worden: ");
            words = Console.ReadLine();
            wordsarray = new char[words.Length];
            foreach (char x in words)
            {
                wordsarray[i] = x;
                i++;
            }
            Console.Write("Geef de encryptiesleutel in (getal): ");
            encryptkey = Convert.ToInt32(Console.ReadLine());

            char[] encrypt = Encrypt(wordsarray, encryptkey);
            Console.Write("\nEncrypt: ");
            for (int c = 0; c < encrypt.Length; c++)
            {
                Console.Write(encrypt[c]);
            }

            char[] decrypt = DeCrypt(encrypt, encryptkey);
            Console.Write("\n\nDecrypt: ");
            for (int d = 0; d < decrypt.Length; d++)
            {
                Console.Write(decrypt[d]);
            }

            Console.ReadKey();
        }

        static char[] DeCrypt(char[] cipertext, int key)
        {
            return Encrypt(cipertext, -key);
        }

        static char[] Encrypt(char[] plaintext, int key)
        {
            char[] result = new char[plaintext.Length];
            //werkt enkel voor kleine letters
            for (int i = 0; i < plaintext.Length; i++)
            {
                if (plaintext[i] == ' ')
                    result[i] = ' ';
                else
                {
                    int newchar = (int)plaintext[i] + key;
                    if (newchar > 122) //nodig voor encrypt
                        newchar -= 26;
                    else if (newchar < 97) //nodig voor decrypt
                        newchar += 26;

                    result[i] = (char)newchar;
                }
            }
            return result;
        }
    }
}
