﻿using System;

namespace H7_arrayoefener1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] getallen = new int[10];
            //Invoer
            Console.WriteLine("Voer 10 gehele getallen in");
            for (int i = 0; i < getallen.Length; i++)
            {
                getallen[i] = Convert.ToInt32(Console.ReadLine());
            }

            //Verwerking (kan ook in lus hierboven ineens)
            int som = 0;
            double gemiddelde = 0;
            int grootste = getallen[0];
            for (int i = 0; i < getallen.Length; i++)
            {
                som += getallen[i]; //som
                if (getallen[i] > grootste)
                    grootste = getallen[i];
            }
            Console.WriteLine("******");
            Console.WriteLine($"Som is {som}, Gemiddelde is {(double)som / getallen.Length}, Grootste getal is {grootste}");
            Console.WriteLine("******");

            Console.WriteLine("Geef minimum getal in?");
            int keuze = Convert.ToInt32(Console.ReadLine());
            if (keuze > grootste)
                Console.WriteLine("Niets is groter");
            else
            {
                for (int i = 0; i < getallen.Length; i++)
                {
                    if (getallen[i] >= keuze)
                        Console.Write($"{getallen[i]},");
                    {

                    }
                }
            }

            Console.ReadKey();
        }
    }
}
