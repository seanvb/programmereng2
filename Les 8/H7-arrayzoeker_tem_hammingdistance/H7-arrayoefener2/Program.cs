﻿using System;

namespace H7_arrayoefener2
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = 5;
            int[] A = VraagVulArray(length);
            int[] B = VraagVulArray(length);
            int[] C = new int[length];

            Console.WriteLine("Array c bevat:");
                
            for (int i = 0; i < length; i++)
            {
                C[i] = A[i] + B[i];
                Console.Write($"{C[i]},");
            }

            Console.ReadKey();
        }

        static int[] VraagVulArray(int lengte)
        {
            int[] array = new int[lengte];
            Console.WriteLine("Voer 5 gehele getallen in");
            for (int i = 0; i < lengte; i++)
            {
                array[i] = Convert.ToInt32(Console.ReadLine());
            }
            return array;
        }
    }
}
