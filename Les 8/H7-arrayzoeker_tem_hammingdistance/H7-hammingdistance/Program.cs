﻿using System;

namespace H7_hammingdistance
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef DNA string 1");
            char[] dna1 = Console.ReadLine().ToCharArray();
            Console.WriteLine("Geef DNA string 2");
            char[] dna2 = Console.ReadLine().ToCharArray();

            if (dna1.Length == dna2.Length && IsRealDNA(dna1) && IsRealDNA(dna2))
            {
                int hamdist = 0;
                for (int i = 0; i < dna1.Length; i++)
                {
                    if (dna1[i] != dna2[i])
                        hamdist++;
                }
                Console.WriteLine($"Distance is {hamdist}");
            }
            else
            {
                Console.WriteLine("Error:DNA strings niet van gelijke lengte of bevat illegale tekens");
            }

            Console.ReadKey();
        }
        private static bool IsRealDNA(char[] dna)
        {
            for (int i = 0; i < dna.Length; i++)
            {
                char tocheck = dna[i];
                if (tocheck != 'G' && tocheck != 'C' & tocheck != 'A' && tocheck != 'T')
                    return false;
            }
            return true;
        }
    }
}
