﻿using System;

namespace H7_arrayzoeker
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] ar = new int[10];
            Random rand = new Random();

            for (int i = 0; i < ar.Length; i++)
            {
                ar[i] = rand.Next(1, 100);
                Console.WriteLine(ar[i]);
            }

            Console.WriteLine("Welk getal moet verwijderd worden?");
            int todel = Convert.ToInt32(Console.ReadLine());

            //index van te zoeken getal zoeken
            int index = -1;
            bool found = false;

            for (int i = 0; i < ar.Length; i++)
            {
                if (!found && ar[i] == todel)
                {
                    found = true;
                    index = i;
                }
            }

            //alle elementen vanaf index met 1 plekje vooruitschuiven
            if (found)
            {
                for (int i = index; i < ar.Length - 1; i++)
                {
                    ar[i] = ar[i + 1];
                }
                ar[ar.Length - 1] = -1; //laatste element op -1
            }

            //Array tonen
            Console.WriteLine("\n\nResultaat: ");
            for (int i = 0; i < ar.Length; i++)
            {
                Console.WriteLine(ar[i]);
            }

            Console.ReadKey();
        }
    }
}
