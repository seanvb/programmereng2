﻿using System;

namespace H7_vraagarray
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] vragen =
            {
                "Hoe oud ben je?",
                "Wat is je postcode?",
                "Hoeveel broers heb je?",
                "Hoeveel zussen heb je?",
                "Wanneer ben je geboren?",
                "Hoeveel is 3+5?"
            };

            int[] antwoorden = new int[vragen.Length];

            for (int i = 0; i < vragen.Length; i++)
            {
                Console.WriteLine(vragen[i]);
                antwoorden[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("\n\nJe antwoorden:");
            for (int i = 0; i < vragen.Length; i++)
            {
                Console.WriteLine($"{vragen[i]}: {antwoorden[i]}");
            }

            Console.ReadKey();
        }
    }
}
